/** Multiplayer communication logic */
const roomName = JSON.parse(document.getElementById('room-name').textContent);

if (window.location.protocol == 'https:') {
    wsProtocol = 'wss://'
} else {wsProtocol = 'ws://'}

const gameSocket = new WebSocket(
    wsProtocol
    + window.location.host
    + '/ws/game/'
    + roomName
    + '/'
    );

gameSocket.onmessage = function(e) {
    const data = JSON.parse(e.data);
    if (data.message.includes('ok')) {
        let move = data.message.replace('ok:', '');
        let move_items = move.split('');
        let player = move_items[0];
        let index = move_items[1];
        applyAction(player, index);
    } else if (data.message.includes('ko')) {
    } else if (data.message.includes('won:')) {
        let winner = data.message.replace('won:', '');
        winnerAction(winner);
    } else if (data.message.includes('draw')) {
        drawAction();
    } 
    console.info(data.message);
};

gameSocket.onclose = function(e) {
    console.error('Game socket closed unexpectedly');
};


function sendMove(index, player) {
    command = `ask:${player}${index}`
    gameSocket.send(JSON.stringify({
            'message': command
        }));
    console.info('Sent', command)
}