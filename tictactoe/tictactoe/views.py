import random

from django.shortcuts import render, redirect
from django.conf import settings
import redis

from . import dino

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=0)


def index(request):
    return render(request, "tictactoe/index.html")

def new(request):
    identifier = dino.dinoipsum()
    # Redirect to playing interface
    response = redirect(f'/play/{identifier}')
    return response

def play(request, room_name):
    # Check a player already connected to this room, 
    # and set the player identifier accordingly
    if redis_instance.exists(room_name):
        current_state = redis_instance.get(room_name).decode()
        # Retrieve previous player kind, from user session:
        if "playing" in request.session and room_name in request.session["playing"]:
            player = request.session["playing"][room_name]["player"]
        else: # Retrieve the player type left unused
            if redis_instance.exists(f"creator_{room_name}"):
                other_player = redis_instance.get(f"creator_{room_name}").decode()
                if other_player == "X":
                    player = "O"
                elif other_player == "O":
                    player = "X"
                else:
                    raise ValueError("Other player should not have other kind than 'X' or 'O'")
            else: # Strange case: here no other user seems to be defined.
                print("Strange things happened in player type handling")
                player = random.choice(['X', 'O'])
                redis_instance.set(f"creator_{room_name}", player)
    else:
        # Create a new redis entry
        current_state = " "*9
        redis_instance.set(room_name, current_state)
        # Define the player type randomly, and update the session and cache accordingly
        player = random.choice(['X', 'O'])
        redis_instance.set(f"creator_{room_name}", player)
    # Ensure session has the proper ojects
    if "playing" not in request.session:
        request.session["playing"] = {}
    if room_name not in request.session["playing"]:
        request.session["playing"][room_name] = {}
    request.session["playing"][room_name]["player"] = player
    # FIXME: Should not allow more than two player to connect
    # should also allow a user to reconnect as wanted.
    return render(request, "tictactoe/multi.html", 
        {"room_name": room_name, "player": player, "current_state": current_state}
    )